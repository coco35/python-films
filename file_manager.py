# Your code goes here.
import json
import calendar


def count(filename):
    """Fonction qui définit le nombre de films"""

    with open(filename, "r", encoding="utf-8") as file:  # ouvrir file.JSON stocké dans file
        list_of_films: list = json.load(file)  # convertit
        number_of_films: int = len(list_of_films)  # compte le nombre de films

    return number_of_films  # retourne le nombre de films de la liste


def written_by(filename, writer) -> list:
    """Fonction qui trouve les films réalisés par un auteur"""

    with open(filename, "r", encoding="utf-8") as file:  # ouvrir file.JSON stocké dans file
        list_of_films: list = json.load(file)  # convertit
        result: list = []

        for i in range(len(list_of_films)):  # parcourt les films
            if list_of_films[i]['Writer'] == writer:  # cherche et retourne les films réalisés par un auteur
                result.append(list_of_films[i])

    return result  # retourne les films réalisés par l'auteur


def longest_title(filename) -> list:
    """Fonction qui trouve le film avec le nom le plus long"""

    with open(filename, "r", encoding="utf-8") as file:  # ouvrir file.JSON stocké dans file
        list_of_films: list = json.load(file)  # convertit
        record: list = list_of_films[0]
        bigger: int = 0

        for i in range(len(list_of_films)):  # parcourt les films
            int_title: int = len(list_of_films[i]['Title'])    # compte le nombre de caracteres
            if list_of_films[i]["Type"] == "movie":  # si c'est un film
                if int_title > bigger:  # si ce titre est le plus long
                    record = list_of_films[i]  # prend le plus grand Year
                    bigger = int_title
    return record  # retourne le plus grand titre de la liste


def best_rating(filename) -> str:
    """Fonction qui trouve l'enregistrement avec le meilleur imdbRating"""

    with open(filename, "r", encoding="utf-8") as file:  # ouvrir file.JSON stocké dans file
        list_of_films: list = json.load(file)  # convertit
        record: str = "aucun film"
        bigger: float = 0
        for i in range(len(list_of_films)):  # parcourt les films
            if "imdbRating" in list_of_films[i]:  # cherche le meilleur imdbRating
                str_imdbRating: str = list_of_films[i]["imdbRating"]    # convertit imdbRating en string
                if str_imdbRating != "N/A":  # si imdbRating n'est pas égal à "N/A"
                    float_imdbRating: float = float(list_of_films[i]["imdbRating"])   # convertit imdbRating en float
                    if float_imdbRating > bigger:  # prend le meilleur imdbRating
                        record = list_of_films[i]
                        bigger = float_imdbRating
    return record  # retourne le meilleur imdbRating


def latest_film(filename) -> str:
    """Cherche l'enregistrement avec le Year le plus récent, retourne le nom du film concerné"""

    with open(filename, "r", encoding="utf-8") as file:  # ouvrir file.JSON stocké dans file
        list_of_films: list = json.load(file)  # convertit
        record: str = "aucun films"
        bigger: int = 0
        for i in range(len(list_of_films)):  # parcourt les films
            year: list = list(list_of_films[i]["Year"])  # transforme "Year" en liste afin de parcourir les caractères
            if list_of_films[i]["Type"] == "movie":  # si c'est un film
                if len(year) > 4 and year[4] == "–":  # si le quatrième caractères est un "-"
                    year.pop(4)  # supprime le "-"
                number_year: int = year[-4] + year[-3] + year[-2] + year[-1]  # prend les 4 derniers caractères (l'année)
                if int(number_year) >= bigger:  # convertit l'année en nombre, prend la plus grande année
                    record = list_of_films[i]["Title"]
                    bigger = int(number_year)
    return record  # retourne la plus grande année


def find_per_genre(filename, genre) -> list:
    """Cherche et retourne les enregistrements ayant le genre Fantasy"""

    result: list = []
    with open(filename, "r", encoding="utf-8") as file:  # ouvrir file.JSON stocké dans file
        list_of_films: list = json.load(file)  # convertit

        for i in range(len(list_of_films)):  # parcourt les films
            if genre in list_of_films[i]["Genre"]:  # recupere les films du genre
                result.append(list_of_films[i])
    return result  # retourne la liste des films du genre


def released_after(filename, date) -> list:
    """Retourne les éléments ayant une release date >= Nov 2015"""

    with open(filename, "r", encoding="utf-8") as file:  # ouvrir file.JSON stocké dans file
        list_of_films: list = json.load(file)  # convertit

        result: list = []

        for i in range(len(list_of_films)):  # parcourt les films
            film_date: str = list_of_films[i]["Released"]
            film_date_list: list = list(film_date.split())
            argument_list_date: list = list(date.split('/'))
            if film_date != 'N/A':  # si la date n'est pas égal à "N/A"
                film_year: int = int(film_date_list[2])
                if film_year > int(argument_list_date[2]):  # si l'annee du film est superieur a l'argument
                    result.append(list_of_films[i])
                if film_year == int(argument_list_date[2]):  # si l'annee du film est egal a l'argument
                    abbr_to_num: dict = {name: num for num, name in enumerate(calendar.month_abbr) if num}
                    film_month: int = abbr_to_num[film_date_list[1]]
                    if film_month > int(argument_list_date[1]):  # si le mois du film est superieur a l'argument
                        result.append(list_of_films[i])   # ajout du film a result
                    if film_month == int(argument_list_date[1]):  # si le mois du film est egal a l'argument
                        film_day: int = int(film_date_list[0])    # ajout du film a result
                        if film_day >= int(argument_list_date[0]):  # si le mois du film superieur ou egal a l'argument
                            result.append(list_of_films[i])   # ajout du film a result
    return result    # retourne result


